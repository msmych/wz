
Vue.filter('uppercase', value => {
	return value.toUpperCase();
});

Vue.component('app-user', {
	data: () => {
  	return {
      users: [
        { username: 'Max' },
        { username: 'Chris' },
        { username: 'Anna' }
      ]
    }
  },
  template: '<div><div class="user" v-for="user in users">Username: {{ user.username }}</div></div>'
});

new Vue({
	el: '#app',
  data: {
  	title: 'Hello world!',
    cssClass: '',
    clicks: 0,
    show: true,
    persons: [
    	{ name: 'Max', age: 27 },
      { name: 'Chris', age: 30 },
      { name: 'Nora', age: 25 }
    ],
    message: 'something',
    username: 'Max'
  },
  methods: {
  	changeTitle() {
    	this.title = 'New title'
    },
    increment() {
    	this.clicks++;
    }
  },
  computed: {
  	counter() {
    	return this.clicks * 2;
    }
  },
  filters: {
  	lowercase: value => {
    	return value.toLowerCase();
    }
  }
})
